<?php

$colorname = array(
    'red'         , 'pink'        , 'purple'      , 'deep purple' ,
    'indigo'      , 'blue'        , 'light blue'  , 'cyan'        ,
    'teal'        , 'green'       , 'light green' , 'lime'        ,
    'yellow'      , 'amber'       , 'orange'      , 'deep orange' ,
    'brown'       , 'grey'        , 'blue grey'
);
$colorcount = count($colorname);

$color['red'         ] = '#ff5252'; $fontcolor['red'         ] = 'white';
$color['pink'        ] = '#ff80ab'; $fontcolor['pink'        ] = 'white';
$color['purple'      ] = '#d500f9'; $fontcolor['purple'      ] = 'white';
$color['deep purple' ] = '#7c4dff'; $fontcolor['deep purple' ] = 'white';
$color['indigo'      ] = '#8c9eff'; $fontcolor['indigo'      ] = 'white';
$color['blue'        ] = '#2196f3'; $fontcolor['blue'        ] = 'white';
$color['light blue'  ] = '#80d8ff'; $fontcolor['light blue'  ] = 'black';
$color['cyan'        ] = '#00bcd4'; $fontcolor['cyan'        ] = 'white';
$color['teal'        ] = '#00bfa5'; $fontcolor['teal'        ] = 'white';
$color['green'       ] = '#4caf50'; $fontcolor['green'       ] = 'white';
$color['light green' ] = '#b2ff59'; $fontcolor['light green' ] = 'black';
$color['lime'        ] = '#eeff41'; $fontcolor['lime'        ] = 'black';
$color['yellow'      ] = '#ffea00'; $fontcolor['yellow'      ] = 'black';
$color['amber'       ] = '#ffc107'; $fontcolor['amber'       ] = 'black';
$color['orange'      ] = '#ffab40'; $fontcolor['orange'      ] = 'black';
$color['deep orange' ] = '#ff6e40'; $fontcolor['deep orange' ] = 'white';
$color['brown'       ] = '#a1887f'; $fontcolor['brown'       ] = 'white';
$color['grey'        ] = '#9e9e9e'; $fontcolor['grey'        ] = 'white';
$color['blue grey'   ] = '#90a4ae'; $fontcolor['blue grey'   ] = 'white';
?>