<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid']) || !isset($_SESSION['tableid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../main'>";
                exit;
            }
            include_once('../settings.html');
        ?>
        <title>수업 추가</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="container">
            <header>
                <h2>수업 추가</h2>
            </header>
            <form action="add_class_process.php" method="post">
                <table>
                    <tr><td colspan="5"><input type="text" name="classname" placeholder="과목명"/></td></tr>
                    <tr><td colspan="5"><input type="text" name="classplace"   placeholder="수업장소"/></td></tr>
                    <tr>
                        <td>
                        <input id="day_mon" type="checkbox" name="classday[]" value="1mon"/>
                        <label for="day_mon">월</label>
                        </td><td>
                        <input id="day_tue" type="checkbox" name="classday[]" value="2tue"/>
                        <label for="day_tue">화</label>
                        </td><td>
                        <input id="day_wed" type="checkbox" name="classday[]" value="3wed"/>
                        <label for="day_wed">수</label>
                        </td><td>
                        <input id="day_thu" type="checkbox" name="classday[]" value="4thu"/>
                        <label for="day_thu">목</label>
                        </td><td>
                        <input id="day_fri" type="checkbox" name="classday[]" value="5fri"/>
                        <label for="day_fri">금</label>
                        </td>
                    </tr>
                </table>
                <table>
                <tr><td rowspan="2" style="width:10%;">~</td><td><select name="classstart">
                    <option value="1">1교시 (오전 9:00 ~ 10:30)</option>
                    <option value="2">2교시 (오전 10:30 ~ 오후 12:00)</option>
                    <option value="3">3교시 (오후 12:00 ~ 1:00)</option>
                    <option value="4">4교시 (오후 1:00 ~ 2:00)</option>
                    <option value="5">5교시 (오후 2:00 ~ 3:30)</option>
                    <option value="6">6교시 (오후 3:30 ~ 5:00)</option>
                    <option value="7">7교시 (오후 5:00 ~ 6:00)</option>
                    <option value="8">8교시 (오후 6:00 ~ 7:00)</option>
                    <option value="9">9교시 (오후 7:00 ~ 8:00)</option>
                    <option value="10">10교시 (오후 8:00 ~ 9:00)</option>
                </select></td></tr>
                    <tr><td><select name="classend">
                    <option value="1">1교시 (오전 9:00 ~ 10:30)</option>
                    <option value="2">2교시 (오전 10:30 ~ 오후 12:00)</option>
                    <option value="3">3교시 (오후 12:00 ~ 1:00)</option>
                    <option value="4">4교시 (오후 1:00 ~ 2:00)</option>
                    <option value="5">5교시 (오후 2:00 ~ 3:30)</option>
                    <option value="6">6교시 (오후 3:30 ~ 5:00)</option>
                    <option value="7">7교시 (오후 5:00 ~ 6:00)</option>
                    <option value="8">8교시 (오후 6:00 ~ 7:00)</option>
                    <option value="9">9교시 (오후 7:00 ~ 8:00)</option>
                    <option value="10">10교시 (오후 8:00 ~ 9:00)</option>
                </select></td></tr>
                <tr><td colspan="2"><select name="classcolor">
                    <?php 
include "../colors.php";
for($i = 0; $i < $colorcount; $i++) {
        echo "<option style='background-color:{$color[$colorname[$i]]};color:{$fontcolor[$colorname[$i]]}'>{$colorname[$i]}</option>";
}
                    ?>
                </select></td></tr>
                </table>
                <button title="뒤로 가기" type="button" onclick="history.back()" class="btn1"><img src="ic_keyboard_backspace_white_48dp.png" alt="뒤로 가기"/></button>
                <button title="리셋" type="reset" class="btn1"><img src="ic_settings_backup_restore_white_48dp.png" alt="리셋"/></button>
                <div class="submitwrapper">
                    <button title="작성 완료" type="submit" class="btn2">
                        <table style="width:auto;margin:auto"><tr><td><img src="ic_done_white_48dp.png" alt="작성 완료"/></td><td style="padding-left:10px;font-size:1.1em;font-weight:bold">추가하기</td></tr></table></button>
                </div>
            </form>
        </div>
    </body>
</html>