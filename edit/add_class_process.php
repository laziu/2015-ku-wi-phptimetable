<?php session_start(); ?>
<!DOCTYPE html>
<title>수업 추가</title>
<link rel="icon" href="../favicon.png"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
    exit;
}
if (!isset($_SESSION['userid']) || !isset($_SESSION['tableid'])) {
    echo "<meta http-equiv='refresh' content='0;url=../main'>";
    exit;
}
if (   !isset($_POST['classname' ]) 
    || !isset($_POST['classplace'])
    || !isset($_POST['classday'  ]) 
    || !isset($_POST['classcolor'])
    || !isset($_POST['classstart']) 
    || !isset($_POST['classend'  ])
    || empty($_POST['classday'])) {
    errmsg("잘못된 접근입니다.");
}

$name  = $_POST['classname' ];
$place = $_POST['classplace'];
$day   = $_POST['classday'  ];
$color = $_POST['classcolor'];
$start = $_POST['classstart'];
$end   = $_POST['classend'  ];

$table_id = $_SESSION['tableid'];
$db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
if (!$db_con)
    errmsg("DB 연결 실패: " . mysqli_connect_error());
mysqli_set_charset($db_con, 'utf8');

$N = count($day);
for($i = 0; $i < $N; $i++) {
    $sql = "INSERT INTO `timetable{$table_id}` (`name`, `place`, `day`, `start`, `end`, `color`) VALUES ('$name', '$place', '{$day[$i]}', '$start', '$end', '$color');";
    if (!mysqli_query($db_con, $sql))
        errmsg("수업 추가 실패: " . mysqli_error($db_con));
}

mysqli_close($db_con);
?>
<meta http-equiv="refresh" content="0;url=edit_table.php"/>