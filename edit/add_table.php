<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../login'>";
                exit;
            }
            include_once('../settings.html'); 
        ?>
        <title>새 시간표 추가</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="container">
            <header>
                <h2>새 시간표 만들기</h2>
            </header>
            <form action="add_table_process.php" method="post">
                <table>
                    <tr><td><input type="text" name="tablename" placeholder="시간표 이름"/></td></tr>
                    <tr><td><input type="text" name="tablesubtitle" placeholder="시간표 부제목"/></td></tr>
                </table>
                <button title="뒤로 가기" type="button" onclick="history.back()" class="btn1"><img src="ic_keyboard_backspace_white_48dp.png" alt="뒤로 가기"/></button>
                <button title="리셋" type="reset" class="btn1"><img src="ic_settings_backup_restore_white_48dp.png" alt="리셋"/></button>
                <div class="submitwrapper">
                    <button title="작성 완료" type="submit" class="btn2">
                        <table style="width:auto;margin:auto"><tr><td><img src="ic_done_white_48dp.png" alt="작성 완료"/></td><td style="padding-left:10px;font-size:1.1em;font-weight:bold">만들기</td></tr></table></button>
                </div>
            </form>
        </div>
    </body>
</html>