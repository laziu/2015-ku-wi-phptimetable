<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../login'>";
                exit;
            }
            include_once('../settings.html'); 
        ?>
        <title>새 시간표 추가</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
function process() {
    if (!isset($_POST['tablename'])) {
        errmsg("시간표 이름이 정해지지 않았습니다.");
        return;
    }
    $title    = $_POST['tablename'];
    $subtitle = $_POST['tablesubtitle'];
    $email_id = str_replace(".", ":", $_SESSION['userid']);
    
    $db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
    if (!$db_con) {
        errmsg("DB 연결 실패: " . mysqli_connect_error());
        return;
    }
    mysqli_set_charset($db_con, 'utf8');
    $sql = "INSERT INTO tablelist (title, subtitle) VALUES\n"
          ."('$title', '$subtitle');";
    if (!mysqli_query($db_con, $sql)) {
        errmsg("시간표 생성 실패: " . mysqli_error($db_con));
        return;
    }
    $sql = "SELECT * from tablelist WHERE title='$title'";
    $result = mysqli_query($db_con, $sql);
    if ($result == false || mysqli_num_rows($result) == 0) {
        errmsg("시간표가 제대로 생성되지 않았습니다: mysql set: " . mysqli_character_set_name($db_con));
        return;
    }
    $row = mysqli_fetch_assoc($result);
    $index = $row['id'];
    $sql = "INSERT INTO `$email_id` (id, name, duration) VALUES\n"
          ."('$index', '$title', '$subtitle')";
    if (!mysqli_query($db_con, $sql)) {
        $ermsg = "세부 설정 실패: " . mysqli_error($db_con);
        $sql = "DELETE FROM `timetable`.`tablelist` WHERE `title`='$title';";
        if (!mysqli_query($db_con, $sql)) 
            $ermsg .= "잔여 설정 초기화 실패: " . mysqli_error($db_con) . "관리자에게 문의 바랍니다.";
        errmsg($ermsg);
        return;
    }
    $sql = "CREATE TABLE `timetable`.`timetable{$index}` (\n"
          ."  `id` INT NOT NULL AUTO_INCREMENT,\n"
          ."  `name` VARCHAR(80) NULL,\n"
          ."  `place` VARCHAR(80) NULL,\n"
          ."  `day` VARCHAR(20) NULL,\n"
          ."  `start` INT NULL,\n"
          ."  `end` INT NULL,\n"
          ."  `color` VARCHAR(20) NULL,\n"
          ."  PRIMARY KEY (`id`))\n"
          ."ENGINE = MyISAM\n"
          ."DEFAULT CHARACTER SET = utf8;";
    if (!mysqli_query($db_con, $sql)) {
        $ermsg = "세부 설정 실패: " . mysqli_error($db_con);
        $sql = "DELETE FROM `timetable`.`tablelist` WHERE `title`='$title';";
        if (!mysqli_query($db_con, $sql)) 
            $ermsg .= "잔여 설정 초기화 실패: " . mysqli_error($db_con) . "관리자에게 문의 바랍니다.";
        $sql = "DELETE FROM `timetable`.`$email_id` WHERE `id`='$index'";
        if (!mysqli_query($db_con, $sql)) 
            $ermsg .= "잔여 설정 초기화 실패: " . mysqli_error($db_con) . "관리자에게 문의 바랍니다.";
        errmsg($ermsg);
        return;
    }
          
    mysqli_close($db_con);
}
process();
        ?>
    </body>
</html>
<meta http-equiv="refresh" content="0;url=../main"/>