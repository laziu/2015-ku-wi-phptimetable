<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid']) || !isset($_SESSION['tableid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../main'>";
                exit;
            }
            include_once('../settings.html');
        ?>
        <title>수업 삭제</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="container">
            <header>
                <h2>수업 삭제</h2>
            </header>
            <form action="delete_class_process.php" method="post">
                <table>
                    <tr><td><select name="deltarget">
                        <?php
$table_id = $_SESSION['tableid'];
$db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
if (!$db_con) {
    errmsg("DB 연결 실패: " . mysqli_connect_error());
    exit;
}
mysqli_set_charset($db_con, 'utf8');
$sql = "SELECT * FROM timetable{$table_id} ORDER BY `name` ASC, `day` ASC, `start` ASC";
$class_result = mysqli_query($db_con, $sql);
include "../colors.php";
for($i = 0; $class_result != false && $class_row = mysqli_fetch_assoc($class_result); $i++) {
    $class_id    = $class_row['id'];
    $class_name  = $class_row['name'];
    $class_day   = $class_row['day'];
    $class_start = $class_row['start'];
    $class_end   = $class_row['end'];
    $class_color = $class_row['color'];
    
    if      (strcmp($class_day, '1mon')==0) $class_day = "월";
    else if (strcmp($class_day, '2tue')==0) $class_day = "화";
    else if (strcmp($class_day, '3wed')==0) $class_day = "수";
    else if (strcmp($class_day, '4thu')==0) $class_day = "목";
    else if (strcmp($class_day, '5fri')==0) $class_day = "금";
        
    if ($class_start == $class_end) $class_time = "{$class_start}교시";
    else $class_time = "{$class_start}교시 ~ {$class_end}교시";
    
    echo "<option value='$class_id' style='background-color:{$color[$class_color]};color:{$fontcolor[$class_color]}'>$class_name &nbsp; &nbsp; ({$class_day}, {$class_time})</option>";
}
mysqli_close($db_con);
                        ?>
                    </select></td></tr>
                </table>
                <button title="뒤로 가기" type="button" onclick="history.back()" class="btn1"><img src="ic_keyboard_backspace_white_48dp.png" alt="뒤로 가기"/></button>
                <div class="submitwrapper">
                    <button title="작성 완료" type="submit" class="btn2">
                        <table style="width:auto;margin:auto"><tr><td><img src="ic_delete_white_48dp.png" alt="작성 완료"/></td><td style="padding-left:10px;font-size:1.1em;font-weight:bold">삭제하기</td></tr></table></button>
                </div>
            </form>
        </div>
    </body>
</html>