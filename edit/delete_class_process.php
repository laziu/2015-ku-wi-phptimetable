<?php session_start(); ?>
<!DOCTYPE html>
<title>수업 삭제</title>
<link rel="icon" href="../favicon.png"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
    exit;
}
if (!isset($_SESSION['userid']) || !isset($_SESSION['tableid'])) {
    echo "<meta http-equiv='refresh' content='0;url=../main'>";
    exit;
}

if (!isset($_POST['deltarget']))
    errmsg("잘못된 접근입니다.");

$target = $_POST['deltarget'];
$table_id = $_SESSION['tableid'];

$db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
if (!$db_con)
    errmsg("DB 연결 실패: " . mysqli_connect_error());
mysqli_set_charset($db_con, 'utf8');

$sql = "DELETE FROM `timetable`.`timetable{$table_id}` WHERE `id`='$target';";
if (!mysqli_query($db_con, $sql))
    errmsg("수업 추가 실패: " . mysqli_error($db_con));

mysqli_close($db_con);
?>
<meta http-equiv="refresh" content="0;url=edit_table.php"/>