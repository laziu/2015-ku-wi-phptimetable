<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../main'>";
                exit;
            }
            include_once('../settings.html');
        ?>
        <title>시간표 삭제</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="container">
            <header>
                <h2>시간표 삭제</h2>
            </header>
            <form action="delete_table_process.php" method="post">
                <table>
                    <tr><td><select name="deltable">
                        <?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
$email_id = str_replace(".", ":", $_SESSION['userid']);
$db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
if (!$db_con)
    errmsg("DB 연결 실패: " . mysqli_connect_error());
mysqli_set_charset($db_con, 'utf8');
$sql = "SELECT * FROM `$email_id` ORDER BY `id` DESC";
$table_result = mysqli_query($db_con, $sql);
if ($table_result == false || mysqli_num_rows($table_result) == 0)
    errmsg("시간표가 없습니다.");

for($i = 0; $table_result != false && $table_row = mysqli_fetch_assoc($table_result); $i++) {
    $table_id    = $table_row['id'];
    $table_name  = $table_row['name'];
    $table_term  = $table_row['duration'];
    
    echo "<option value='$table_id'>$table_name ({$table_term})</option>";
}
mysqli_close($db_con);
                        ?>
                    </select></td></tr>
                </table>
                <button title="뒤로 가기" type="button" onclick="history.back()" class="btn1"><img src="ic_keyboard_backspace_white_48dp.png" alt="뒤로 가기"/></button>
                <div class="submitwrapper">
                    <button title="작성 완료" type="submit" class="btn2">
                        <table style="width:auto;margin:auto"><tr><td><img src="ic_delete_white_48dp.png" alt="작성 완료"/></td><td style="padding-left:10px;font-size:1.1em;font-weight:bold">삭제하기</td></tr></table></button>
                </div>
            </form>
        </div>
    </body>
</html>