<?php session_start(); ?>
<!DOCTYPE html>
<title>수업 삭제</title>
<link rel="icon" href="../favicon.png"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
    exit;
}
if (!isset($_SESSION['userid'])) {
    echo "<meta http-equiv='refresh' content='0;url=../main'>";
    exit;
}

if (!isset($_POST['deltable']))
    errmsg("잘못된 접근입니다.");

$email_id = str_replace(".", ":", $_SESSION['userid']);
$target = $_POST['deltable'];

$db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
if (!$db_con)
    errmsg("DB 연결 실패: " . mysqli_connect_error());
mysqli_set_charset($db_con, 'utf8');

$sql = "DROP TABLE `timetable`.`timetable{$target}`";
if (!mysqli_query($db_con, $sql))
    errmsg("삭제 실패: " . mysqli_error($db_con));
$sql = "DELETE FROM `timetable`.`tablelist` WHERE `id`='$target'";
if (!mysqli_query($db_con, $sql))
    errmsg("삭제 실패: " . mysqli_error($db_con));
$sql = "DELETE FROM `timetable`.`{$email_id}` WHERE `id`='$target'";
if (!mysqli_query($db_con, $sql))
    errmsg("삭제 실패: " . mysqli_error($db_con));

mysqli_close($db_con);
?>
<meta http-equiv="refresh" content="0;url=../main"/>