<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid']) || !isset($_SESSION['tableid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../main'>";
                exit;
            }
            include_once('../settings.html'); 
        ?>
        <title>시간표 수정</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <link rel="stylesheet" type="text/css" href="edit_table_style.css"/>
    </head>
    <body>
        <form action="edit_table_process.php" method="post">
            <header>
                <h2>시간표 수정</h2>
            </header>
            <?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
function process() {
    $table_id = $_SESSION['tableid'];
    $db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
    if (!$db_con) {
        errmsg("DB 연결 실패: " . mysqli_connect_error());
        return;
    }
    mysqli_set_charset($db_con, 'utf8');
    $sql = "SELECT * FROM timetable{$table_id} ORDER BY `name` ASC, `day` ASC, `start` ASC";
    $class_result = mysqli_query($db_con, $sql);
    
    for($i = 0; $class_result != false && $class_row = mysqli_fetch_assoc($class_result); $i++) {
        $class_id    = $class_row['id'];
        $class_name  = $class_row['name'];
        $class_place = $class_row['place'];
        $class_day   = $class_row['day'];
        $class_start = $class_row['start'];
        $class_end   = $class_row['end'];
        $class_color = $class_row['color'];
        
        echo "<div class='container'>"
            ."<div class='smallheader'>#{$i}</div>"
            ."<div class='container2'>\n"
            ."<input type='hidden' name='cls{$i}id' value='$class_id'/>\n"
                ."<table>\n"
                    ."<tr><td colspan='3'><input type='text' name='cls{$i}name' placeholder='과목명' value='$class_name'/></td></tr>\n"
                    ."<tr><td colspan='3'><input type='text' name='cls{$i}place' placeholder='수업장소' value='$class_place'/></td></tr>\n"
                    ."<tr><td rowspan='2' class='day'><select name='cls{$i}day'>\n"
                        ."<option value='1mon' ".(strcmp($class_day, '1mon') == 0? 'selected' : '').">월</option>\n"
                        ."<option value='2tue' ".(strcmp($class_day, '2tue') == 0? 'selected' : '').">화</option>\n"
                        ."<option value='3wed' ".(strcmp($class_day, '3wed') == 0? 'selected' : '').">수</option>\n"
                        ."<option value='4thu' ".(strcmp($class_day, '4thu') == 0? 'selected' : '').">목</option>\n"
                        ."<option value='5fri' ".(strcmp($class_day, '5fri') == 0? 'selected' : '').">금</option>\n"
                    ."</select></td><td class='time'>시작</td><td><select name='cls{$i}start'>\n"
                        ."<option value='1' ".(strcmp($class_start, 1)==0?'selected' : '').">1교시 (오전 9:00 ~ 10:30)</option>\n"
                        ."<option value='2' ".(strcmp($class_start, 2)==0?'selected' : '').">2교시 (오전 10:30 ~ 오후 12:00)</option>\n"
                        ."<option value='3' ".(strcmp($class_start, 3)==0?'selected' : '').">3교시 (오후 12:00 ~ 1:00)</option>\n"
                        ."<option value='4' ".(strcmp($class_start, 4)==0?'selected' : '').">4교시 (오후 1:00 ~ 2:00)</option>\n"
                        ."<option value='5' ".(strcmp($class_start, 5)==0?'selected' : '').">5교시 (오후 2:00 ~ 3:30)</option>\n"
                        ."<option value='6' ".(strcmp($class_start, 6)==0?'selected' : '').">6교시 (오후 3:30 ~ 5:00)</option>\n"
                        ."<option value='7' ".(strcmp($class_start, 7)==0?'selected' : '').">7교시 (오후 5:00 ~ 6:00)</option>\n"
                        ."<option value='8' ".(strcmp($class_start, 8)==0?'selected' : '').">8교시 (오후 6:00 ~ 7:00)</option>\n"
                        ."<option value='9' ".(strcmp($class_start, 9)==0?'selected' : '').">9교시 (오후 7:00 ~ 8:00)</option>\n"
                        ."<option value='10' ".(strcmp($class_start, 10)==0?'selected' : '').">10교시 (오후 8:00 ~ 9:00)</option>\n"
                    ."</select></td></tr>\n"
            ."<tr><td class='time'>끝</td><td><select name='cls{$i}end'>\n"
                        ."<option value='1' ".(strcmp($class_end, 1)==0?'selected' : '').">1교시 (오전 9:00 ~ 10:30)</option>\n"
                        ."<option value='2' ".(strcmp($class_end, 2)==0?'selected' : '').">2교시 (오전 10:30 ~ 오후 12:00)</option>\n"
                        ."<option value='3' ".(strcmp($class_end, 3)==0?'selected' : '').">3교시 (오후 12:00 ~ 1:00)</option>\n"
                        ."<option value='4' ".(strcmp($class_end, 4)==0?'selected' : '').">4교시 (오후 1:00 ~ 2:00)</option>\n"
                        ."<option value='5' ".(strcmp($class_end, 5)==0?'selected' : '').">5교시 (오후 2:00 ~ 3:30)</option>\n"
                        ."<option value='6' ".(strcmp($class_end, 6)==0?'selected' : '').">6교시 (오후 3:30 ~ 5:00)</option>\n"
                        ."<option value='7' ".(strcmp($class_end, 7)==0?'selected' : '').">7교시 (오후 5:00 ~ 6:00)</option>\n"
                        ."<option value='8' ".(strcmp($class_end, 8)==0?'selected' : '').">8교시 (오후 6:00 ~ 7:00)</option>\n"
                        ."<option value='9' ".(strcmp($class_end, 9)==0?'selected' : '').">9교시 (오후 7:00 ~ 8:00)</option>\n"
                        ."<option value='10' ".(strcmp($class_end, 10)==0?'selected' : '').">10교시 (오후 8:00 ~ 9:00)</option>\n"
                    ."</select></td></tr>\n"
                    ."<tr><td colspan='3'><select name='cls{$i}color'>\n";
        include "../colors.php";
        for($j = 0; $j < $colorcount; $j++) {
            echo "<option style='background-color:{$color[$colorname[$j]]};color:{$fontcolor[$colorname[$j]]}' ".(strcmp($class_color, $colorname[$j]) == 0? 'selected' : '').">{$colorname[$j]}</option>\n";
        }
        echo         "</select></td></tr>\n"
                ."</table>\n"
            ."</div></div>\n";
    }
    if ($i == 0)
        echo "<p style='margin:1em'>수업이 없습니다.</p>";
    echo "<input type='hidden' name='classcount' value='$i'/>";
    mysqli_close($db_con);
}
process();
            ?>
            <div class="bottombar">
                <button title="뒤로 가기" type="button" onclick="history.back()" class="btn1"><img src="ic_keyboard_backspace_white_48dp.png" alt="뒤로 가기"/></button>
                <button title="수업 추가" type="button" onclick="addclass()" class="btn1"><img src="ic_add_white_48dp.png" alt="수업 추가"/></button>
                <button title="수업 삭제" type="button" onclick="delclass()" class="btn1"><img src="ic_delete_white_48dp.png" alt="수업 삭제"/></button>
                <div class="submitwrapper">
                    <button title="수정 완료" type="submit" class="btn2">
                        <table style="width:auto;margin:auto"><tr><td><img src="ic_done_white_48dp.png" alt="완료"/></td><td style="font-size:1.1em;font-weight:bold">완료</td></tr></table></button>
                </div>
            </div>
        </form>
        <script>
            function addclass() { location.href = "add_class.php"; }
            function delclass() { location.href = "delete_class.php"; }
        </script>
    </body>
</html>