<?php session_start(); ?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
/*
if (!isset($_SESSION['userid']) || !isset($_SESSION['tableid'])) {
    echo "<meta http-equiv='refresh' content='0;url=../main'>";
    exit;
}
*/          
$tableid = $_SESSION['tableid'];

$db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
if (!$db_con) {
    echo "<script>alert('DB 연결 실패: " . mysqli_connect_error() . "');history.back();</script>";
    return;
}
mysqli_set_charset($db_con, 'utf8');

function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
    mysqli_close($db_con);
    exit;
}
function getpost($arg, $msg) {
    if (!isset($_POST[$arg]))
        errmsg($msg);
    return $_POST[$arg];
}
function getdata($id, $arg) {
    if (!isset($_POST["cls{$id}{$arg}"])) {
        if      (strcmp($arg, 'name' )==0) $arr = "과목명이";
        else if (strcmp($arg, 'place')==0) $arr = "수업장소가";
        else if (strcmp($arg, 'day'  )==0) $arr = "요일이";
        else if (strcmp($arg, 'start')==0) $arr = "시작하는 교시가";
        else if (strcmp($arg, 'end'  )==0) $arr = "끝나는 교시가";
        else if (strcmp($arg, 'color')==0) $arr = "색상이";
        else if (strcmp($arg, 'id'   )==0) $arr = "ID가";    
            errmsg("#{$id}에서 {$arr} 정해지지 않았습니다.");
    }
    return $_POST["cls{$id}{$arg}"];
}

$class_count = getpost("classcount", "잘못된 접근입니다.");

for($i = 0; $i < $class_count; $i++) {
    $id    = getdata($i, 'id');
    $name  = getdata($i, 'name');
    $place = getdata($i, 'place');
    $day   = getdata($i, 'day');
    $start = getdata($i, 'start');
    $end   = getdata($i, 'end');
    $color = getdata($i, 'color');
    
    $sql = "UPDATE `timetable`.`timetable{$tableid}` SET `name`='$name', `place`='$place', `day`='$day', `start`='$start', `end`='$end', `color`='$color' WHERE `id`='$id';";
    if (!mysqli_query($db_con, $sql)) 
        errmsg("mysql error: $id 행을 수정할 수 없습니다.");
}
mysqli_close($db_con);
?>
<meta http-equiv="refresh" content="0;url=../main"/>