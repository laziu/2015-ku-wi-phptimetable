<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../login'>";
                exit;
            }
            include_once('../settings.html'); 
        ?>
        <title>시간표 수정</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="container">
            <header>
                <h2>수정할 시간표 선택</h2>
            </header>
            <form action="select_table_process.php" method="post">
                <select name="tableid" style="margin-bottom:15px;">
                    <?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
function process() {
    $email_id = str_replace(".", ":", $_SESSION['userid']);
    $db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
    if (!$db_con) {
        errmsg("DB 연결 실패: " . mysqli_connect_error());
        return;
    }
    mysqli_set_charset($db_con, 'utf8');
    $sql = "SELECT * FROM `$email_id`";
    $table_result = mysqli_query($db_con, $sql);
    if ($table_result == false || mysqli_num_rows($table_result) == 0) {
        errmsg("시간표가 없습니다.");
        return;
    }
    
    while($table_row = mysqli_fetch_assoc($table_result)) {
        $table_id   = $table_row['id'];
        $table_name = $table_row['name'];
        echo "<option value='$table_id'>$table_name</option>";
    }
    mysqli_close($db_con);
}
process();
                    ?>
                </select>
                <button title="뒤로 가기" type="button" onclick="history.back()" class="btn1"><img src="ic_keyboard_backspace_white_48dp.png" alt="뒤로 가기"/></button>
                <div class="submitwrapper">
                    <button title="작성 완료" type="submit" class="btn2">
                        <table style="width:auto;margin:auto"><tr><td><img src="ic_done_white_48dp.png" alt="선택"/></td><td style="padding-left:10px;font-size:1.1em;font-weight:bold">선택</td></tr></table></button>
                </div>
            </form>
        </div>
    </body>
</html>