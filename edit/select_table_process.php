<?php session_start(); ?>
<!DOCTYPE html>
<title>시간표 선택</title>
<link rel="icon" href="../favicon.png"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
if (!isset($_SESSION['userid']) || !isset($_POST['tableid'])) {
    echo "<meta http-equiv='refresh' content='0;url=../login'>";
    exit;
}
$_SESSION['tableid'] = $_POST['tableid'];
?>
<meta http-equiv="refresh" content="0;url=edit_table.php"/>