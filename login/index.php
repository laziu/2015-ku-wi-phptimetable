<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php include_once('../settings.html'); ?>
        <title>로그인</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="cardform" id="loginform">
            <h3>시간표를 사용하려면 로그인하세요.</h3>
            <img src="../logo.png" alt="http://2014.barcamp.ch/wp-content/uploads/sites/2/2014/08/timetable-512.png"/>
            <form action="login_check.php" method="post">
            <p>
                <input name="email" type="email" placeholder="이메일"/><br/>
                <input name="paswd" type="password" placeholder="비밀번호"/>
            </p>
            <button type="submit" class="submit">로그인</button><br/>
            </form>
            <div id="more">
                <p>아직 등록하지 않으셨어요? <a href="register.php">회원가입</a>
                    <!--<br/>
비밀번호를 잊어버리셨어요? <button onclick="findpasswd()">비밀번호 찾기</button>--></p>
            </div>
        </div>
    </body>
</html>