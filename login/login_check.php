<?php session_start(); ?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
function process() {
    if (!isset($_POST['email']) || !isset($_POST['paswd'])) 
        return;
    $email = $_POST["email"];
    $paswd = $_POST["paswd"];
    $passlen = strlen($paswd);
    if ($passlen < 6 || 30 < $passlen) {
        errmsg("비밀번호의 길이가 올바르지 않습니다.");
        return;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        errmsg("이메일의 형식이 맞지 않습니다.");
        return;
    }
    $db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
    if (!$db_con) {
        errmsg("DB 연결 실패: " . mysqli_connect_error());
        return;
    }
    mysqli_set_charset($db_con, 'utf8');
    $sql = "SELECT email, password FROM usergroup WHERE email = '$email'";
    $result = mysqli_query($db_con, $sql);
    if (mysqli_num_rows($result) == 0) {
        errmsg("가입된 이메일이 아닙니다.");
        return;
    }
    $row = mysqli_fetch_assoc($result);
    if ($row == 0) {
        errmsg("가입된 이메일이 아닙니다.");
        return;
    }
    if ($paswd !== $row["password"]) {
        errmsg("비밀번호가 맞지 않습니다.");
        return;
    }
    mysqli_close($db_con);
    $_SESSION['userid'] = $email;
}

process();
?>
<meta http-equiv="refresh" content="0;url=../main"/>
