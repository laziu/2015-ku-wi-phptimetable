<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php include_once('../settings.html'); ?>
        <title>회원가입</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="cardform" id="registerform">
            <p>* 비밀번호는 6 ~ 30 자리이여야 합니다.</p>
            <form action="register_check.php" method="post">
                <p>
                    <input name="email" type="email" placeholder="이메일"/><br/>
                    <input name="paswd" type="password" placeholder="비밀번호"/>
                </p>
            <button type="submit" class="submit">회원가입</button><br/>
            </form>
        </div>
    </body>
</html>