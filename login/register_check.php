<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<?php
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
function process() {
    if (!isset($_POST['email']) || !isset($_POST['paswd'])) 
        return;
    $email = $_POST["email"];
    $paswd = $_POST["paswd"];
    $passlen = strlen($paswd);
    if ($passlen < 6 || 30 < $passlen) {
        errmsg("비밀번호의 길이가 올바르지 않습니다.");
        return;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        errmsg("이메일의 형식이 맞지 않습니다.");
        return;
    }
    if (strlen($email) > 60) {
        errmsg("이메일의 길이가 너무 깁니다. (60자 제한)");
        return;
    }
    
    $db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
    if (!$db_con) {
        errmsg("DB 연결 실패: " . mysqli_connect_error());
        return;
    }
    mysqli_set_charset($db_con, 'utf8');
    $sql = "SELECT email FROM usergroup WHERE email = '$email'";
    $result = mysqli_query($db_con, $sql);
    if (mysqli_num_rows($result) != 0) {
        errmsg("이미 가입되어 있는 이메일입니다.");
        return;
    }
    $sql = "INSERT INTO usergroup\nVALUES ('$email', '$paswd')";
    if (!mysqli_query($db_con, $sql)) {
        errmsg("아이디 생성 실패: " . mysqli_error($db_con));
        return;
    }
    $email_id = str_replace(".", ":", $email);
    $sql = "CREATE TABLE `timetable`.`{$email_id}` (\n"
          ."  `id` INT NOT NULL,\n"
          ."  `name` VARCHAR(80) NULL,\n"
          ."  `duration` VARCHAR(40) NULL,\n"
          ."  PRIMARY KEY (`id`))\n"
          ."ENGINE = MyISAM\n"
          ."DEFAULT CHARACTER SET = utf8;";
    if (!mysqli_query($db_con, $sql)) {
        $ermsg = "세부 설정 실패: " . mysqli_error($db_con);
        $sql = "DELETE FROM `timetable`.`usergroup` WHERE `email`='$email';";
        if (!mysqli_query($db_con, $sql)) 
            $ermsg .= "잔여 설정 초기화 실패: " . mysqli_error($db_con) . "관리자에게 문의 바랍니다.";
        errmsg($ermsg);
        return;
    }

    echo "<script>alert('가입되었습니다.');</script>";
    mysqli_close($db_con);
}

process();
?>
<meta http-equiv="refresh" content="0;url=./"/>