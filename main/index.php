<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <?php
            if (!isset($_SESSION['userid'])) {
                echo "<meta http-equiv='refresh' content='0;url=../login'>";
                exit;
            }
            if (isset($_SESSION['tableid']))
                unset($_SESSION['tableid']);
            include_once('../settings.html');
        ?>
        <title>시간표</title>
        <link rel="icon" href="../favicon.png"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <header>
            <ul>
                <li><?php echo $_SESSION['userid']; ?></li>
                <li>
                    <ul>
                        <li><a href="../edit/add_table.php" title="새 시간표 추가"><img src="ic_add_white_48dp.png" alt="새 시간표 추가"/></a></li>
                        <li><a href="../edit/select_table.php" title="시간표 수정"><img src="ic_create_white_48dp.png" alt="시간표 수정"/></a></li>
                        <li><a href="../edit/delete_table.php" title="시간표 삭제"><img src="ic_delete_white_48dp.png" alt="시간표 삭제"/></a></li>
                        <li><a href="../login/logout.php" title="로그아웃"><img src="ic_exit_to_app_white_48dp.png" alt="로그아웃"/></a></li>
                    </ul>
                </li>
            </ul>
        </header>
        <?php 
function errmsg($err) {
    echo "<script>alert('$err');history.back();</script>";
}
function process() {
    $email = $_SESSION['userid'];
    $email_id = str_replace(".", ":", $email);
    
    $db_con = mysqli_connect("localhost", "timetable", "1313240db", "timetable");
    if (!$db_con) {
        errmsg("DB 연결 실패: " . mysqli_connect_error());
        return;
    }
    mysqli_set_charset($db_con, 'utf8');
    $sql = "SELECT * FROM `$email_id` ORDER BY `id` DESC";
    $table_result = mysqli_query($db_con, $sql);
    if ($table_result == false || mysqli_num_rows($table_result) == 0) {
        echo "<p style='margin: 1.5em'>시간표가 없습니다.</p>";
        return;
    }
    while($table_row = mysqli_fetch_assoc($table_result)) {
        $table_id   = $table_row['id'];
        $table_name = $table_row['name'];
        $table_term = $table_row['duration'];
        
        $sql = "SELECT * FROM timetable{$table_id}";
        $class_result = mysqli_query($db_con, $sql);
        
        for($i = 1; $i <= 5; $i++)
            for($j = 1; $j <= 10; $j++)
                $table_class[$i][$j] = 0;
        
        while($class_row = mysqli_fetch_assoc($class_result)) {
            $class_name  = $class_row['name'];
            $class_place = $class_row['place'];
            $class_day   = $class_row['day'];
            $class_start = $class_row['start'];
            $class_end   = $class_row['end'];
            $class_color = $class_row['color'];
            
            if      (strcmp($class_day, '1mon') == 0) $class_day = 1;
            else if (strcmp($class_day, '2tue') == 0) $class_day = 2;
            else if (strcmp($class_day, '3wed') == 0) $class_day = 3;
            else if (strcmp($class_day, '4thu') == 0) $class_day = 4;
            else if (strcmp($class_day, '5fri') == 0) $class_day = 5;
                        
            $table_class[$class_day][$class_start] = array(
                'name'  => $class_name,
                'place' => $class_place,
                'rows'  => $class_end - $class_start + 1,
                'color' => $class_color
            );
            for($i = $class_start + 1; $i <= $class_end; $i++)
                $table_class[$class_day][$i] = 1;
        }
        
        echo "<div class='content'>"
                ."<div class='headline'>"
                    ."<span class='tablename'>$table_name</span>"
                    ."<span class='tableterm'>$table_term</span>"
                ."</div>"
                ."<table>"
                    ."<tr><td></td>"
                    ."<td><b>월</b></td><td><b>화</b></td><td><b>수</b></td><td><b>목</b></td><td><b>금</b></td>"
                    ."</tr>";
        
        $clstime[1] = '9:00 ~ 10:30';
        $clstime[2] = '10:30 ~ 12:00';
        $clstime[3] = '12:00 ~ 1:00';
        $clstime[4] = '1:00 ~ 2:00';
        $clstime[5] = '2:00 ~ 3:30';
        $clstime[6] = '3:30 ~ 5:00';
        $clstime[7] = '5:00 ~ 6:00';
        $clstime[8] = '6:00 ~ 7:00';
        $clstime[9] = '7:00 ~ 8:00';
        $clstime[10] = '8:00 ~ 9:00';
        
        include "../colors.php";
        
        for($i = 1; $i <= 10; $i++) {
            echo "<tr><td>{$i}교시<br/><span class'small'>$clstime[$i]</span></td>";
            for($j = 1; $j <= 5; $j++) {
                if ($table_class[$j][$i] == 0)
                    echo "<td></td>";
                else if ($table_class[$j][$i] != 1) {
                    $ename  = $table_class[$j][$i]['name'];
                    $eplace = $table_class[$j][$i]['place'];
                    $erows  = $table_class[$j][$i]['rows'];
                    $ecolor = $table_class[$j][$i]['color'];
                    echo "<td rowspan='$erows' style='background-color:{$color[$ecolor]};color:{$fontcolor[$ecolor]}'>$ename<br/><span class='small'>$eplace</span></td>";
                }
            }
            echo "</tr>";
        }
        echo "</table></div>";
    }
    mysqli_close($db_con);
}

process();
        ?>
    </body>
</html>