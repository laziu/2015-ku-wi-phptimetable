<meta charset="utf-8"/>
<style>table,td{border:1px solid black;border-collapse: collapse;}</style>

<?php
$a = 1;
$sum = 0;

while($a<=10)
    $sum += $a++;

echo "1에서 10까지 자연수의 합은 $sum 입니다.<br/>";
?>

<hr/>

<?php
for($i=1; $i <= 10; $i++)
    echo $i."<br/>";
?>

<hr/>

<table>
<?php
for($i=1; $i <= 10; $i++)
    echo "<tr><td> $i </td><td>...</td></tr>";
?>
</table>

<hr/>

<?php
$score[0]=80;
$score[1]=90;
$score[2]=90;
$score[3]=99;
$score[4]=78;

$sum=0;
for($i=0; $i<=4; $i++)
    $sum += $score[$i];

$avg = $sum/5;

echo "과목 점수 : ";
for($i = 0; $i < 4; $i++)
    echo "{$score[$i]}, ";
echo "{$score[$i]}<br/>";
echo "합계 : $sum, 평균 : $avg<br/>";
?>